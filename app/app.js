var moviesApp = angular.module('myImdbApp', ['ngRoute']);

moviesApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: './movies.html'
            }).
            when('/about', {
                templateUrl: './about.html'
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);