angular.module('myImdbApp').controller('moviesCtrl', function($scope, $http, $location) {
    $scope.data = [];
    $scope.movies = [];
    $scope.editingMode = false;
    $scope.movie = {};
    $scope.homeActive = true;
    $scope.searchAll = true;
    $scope.numMovies = 0;
    $scope.pageSize = 5;
    $scope.numPages = 0;
    $scope.currentPage = 0;
    $scope.firstLoad = true;

    $scope.searchField = {};

    $scope.$on('$locationChangeSuccess', function(){
       $scope.homeActive = ($location.url() === '/');
    });

    $scope.$on('$viewContentLoaded', function(){
        if ($scope.firstLoad) {
            $scope.getData();
            $scope.firstLoad = false;
        }
    });

    $scope.enterSend = function(event, searchField) {
        if (event.which == 13 && searchField && searchField.val && searchField.val.length>0) {
            $scope.getData(searchField);
        }
    };

    $scope.getData = function(searchField){
        $http.get('./backend.php'+((searchField && searchField.val && (typeof(searchField.val)!=='undefined'))?"?title="+searchField.val:"")).
            success(function(data, status, headers, config) {
                if ((data instanceof Array)) {
                    $scope.movies = data;
                    $scope.currentPage = 1;
                    setCurrentPageData();
                } else {
                    $scope.movies = [];
                    $scope.currentPage = 1;
                    setCurrentPageData();
                }
            }).
            error(function(data, status, headers, config) {
                showModalAlert("No se han podido cargar los datos, inténtelo más tarde.", "main-alert");
            });
    };

    $scope.add = function(){
        $http.post('./backend.php', $scope.movie).
            success(function(data, status, headers, config) {
                $scope.hideModal();
                $scope.getData($scope.searchField);
            }).
            error(function(data, status, headers, config) {
                showModalAlert("No se ha podido añadir, inténtelo más tarde.");
            });
    };

    $scope.setSearchAll = function(value){
        $scope.searchAll = value;
        if (value) {
            $scope.searchField = {};
            $scope.getData();
        }
    };

    $scope.pager = function(value){
        $scope.currentPage += value;
        setCurrentPageData();
    };


    $scope.editModal = function(movie){
        $scope.editingMode = true;
        angular.copy(movie, $scope.movie);
    };


    $scope.edit = function(){
        $http.put('./backend.php', $scope.movie).
            success(function(data, status, headers, config) {
                $scope.hideModal();
                $scope.getData($scope.searchField);
            }).
            error(function(data, status, headers, config) {
                showModalAlert("No se ha podido editar, inténtelo más tarde.");
            });
    };

    $scope.hideModal = function(){
        if ($scope.editingMode) {
            $scope.editingMode = false;
        }
        $scope.movie = {};
        $('#myModal').modal('hide');
    };

    $scope.hideDeleteModal = function(){
        $scope.movie = {};
        $('#deleteModal').modal('hide');
    };


    $scope.deleteModal = function(movie){
        $scope.movie = movie;
        $('#deleteModal').modal('show');
    };

    $scope.delete = function(){
        $http.delete('./backend.php?id='+$scope.movie.id).
            success(function(data, status, headers, config) {
                $scope.hideDeleteModal();
                $scope.getData($scope.searchField);
            }).
            error(function(data, status, headers, config) {
                showModalAlert("No se ha podido eliminar, inténtelo más tarde.", "delete-modal-alert");
            });
    };

    $scope.hideResetModal = function(){
        $('#resetModal').modal('hide');
    };

    $scope.resetModal = function(movie){
        $('#resetModal').modal('show');
    };

    $scope.resetDB = function(){
        $http.post('./backend.php?reset=true').
            success(function(data, status, headers, config) {
                $scope.hideResetModal();
                $scope.getData();
            }).
            error(function(data, status, headers, config) {
                showModalAlert("No se ha podido reiniciar la BBDD, inténtelo más tarde.", "reset-modal-alert");
            });
    };

    function setCurrentPageData(){
        $scope.numMovies = $scope.movies.length;
        $scope.numPages = Math.floor($scope.numMovies / $scope.pageSize) + (($scope.numMovies % $scope.pageSize > 0) ? 1 : 0);
        $scope.data = $scope.movies.slice(($scope.currentPage-1)*$scope.pageSize, $scope.currentPage*$scope.pageSize);
    };

    function cleanAlerts() {
        $(".alert-dismissable").remove();
    }

    function showModalAlert(msg, idSelector) {
        cleanAlerts();
        var html = "<div class='alert alert-danger alert-dismissable'>"+
            "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>" +
            "<strong>" + msg + "</strong>" +
            "</div>";
        if(typeof(idSelector)==='undefined') {
            $(html).appendTo($('#modal-alert'));
        } else {
            $(html).appendTo($('#'+idSelector));
        }
    }

    $('#myModal').on('show.bs.modal', function (e) {
        cleanAlerts();
    })
});


