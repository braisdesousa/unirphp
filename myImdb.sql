CREATE SCHEMA IF NOT EXISTS `myImdb`;

CREATE TABLE IF NOT EXISTS `movies` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `year` int(11) NOT NULL,
  `director` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `synopsis` text COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

INSERT INTO `movies` (`title`, `year`, `director`, `synopsis`) VALUES
('La vita è bella', 1999, 'Roberto Benigni', 'A Jewish man has a wonderful romance with the help of his humour, but must use that same quality to protect his son in a Nazi death camp.'),
('Il buono, il brutto, il cattivo.', 1968, 'Sergio Leone', 'A bounty hunting scam joins two men in an uneasy alliance against a third in a race to find a fortune in gold buried in a remote cemetery.'),
('The Thin Red Line', 1998, 'Terrence Malick', 'Terrence Malick''s adaptation of James Jones'' autobiographical 1962 novel, focusing on the conflict at Guadalcanal during the second World War.'),
('Cidade de Deus', 2002, 'Fernando Meirelles', 'Two boys growing up in a violent neighborhood of Rio de Janeiro take different paths: one becomes a photographer, the other a drug dealer.'),
('Forrest Gump', 1994, 'Robert Zemeckis', 'Forrest Gump, while not intelligent, has accidentally been present at many historic moments, but his true love, Jenny Curran, eludes him.'),
('The Godfather', 1972, 'Francis Ford Coppola', 'The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.'),
('Schindler''s List', 1994, 'Steven Spielberg', 'In Poland during World War II, Oskar Schindler gradually becomes concerned for his Jewish workforce after witnessing their persecution by the Nazis.'),
('12 Angry Men', 1958, 'Sidney Lumet', 'A dissenting juror in a murder trial slowly manages to convince the others that the case is not as obviously clear as it seemed in court.'),
('The Shawshank Redemption', 1995, 'Frank Darabont', 'Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.'),
('The Pursuit of Happyness', 2007, 'Gabriele Muccino', 'A struggling salesman takes custody of his son as he''s poised to begin a life-changing professional endeavor.');