<?php
    include_once('PHP-MySQL-Class/class.MySQL.php');
    include_once('DatabaseQueries.php');

    // ========================================================================================================================
    // Params
    // ========================================================================================================================

    $db_user = "unir";
    $db_password = "sousas404";
    $db_name = "myIMDB";
    $db_hostname = "localhost";
    $table_name = "movies";

    // ========================================================================================================================
    // Aux. functions
    // ========================================================================================================================

    function getRequestPayload(){
        $request_body = file_get_contents('php://input');
        return get_object_vars(json_decode($request_body));
    }
    function isAssoc($arr)
    {
    return array_keys($arr) !== range(0, count($arr) - 1);
    }
    function yearToNumber($results) {
        if (is_array($results)) {
            foreach ($results as $key => $row) {
                $results[$key]['year'] = (int)$results[$key]['year'];
            }
        }
        return $results;
    }

    function sendResponse($response) {
        if ($response === false) {
            header('Content-Type: application/json', true, 500);
            $response['action'] = $_SERVER['REQUEST_METHOD'];
            $response['query'] = $_REQUEST;
            $response['info'] = "Failed";
            echo json_encode($response);
        } else {
            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    // ========================================================================================================================
    // Database connection
    // ========================================================================================================================

    $queries= new DatabaseQueries($db_name, $table_name);
    $db = new MySQL($db_name, $db_user, $db_password, $db_hostname);
    if (($db->executeSQL($queries->databaseExists)===true)) {
            $db->ExecuteSQL($queries->createDatabase);
            $db->closeConnection();
            $db = new MySQL($db_name, $db_user, $db_password, $db_hostname);
            $db->ExecuteSQL($queries->createTable);
            $db->ExecuteSQL($queries->insertDefaultRows);
    }

    // ========================================================================================================================
    // Main
    // ========================================================================================================================

    if ($_SERVER['REQUEST_METHOD'] === "GET") {
        $where = (is_array($_REQUEST) && array_key_exists('title',$_REQUEST))?$_REQUEST:NULL;
        $response = ($db->Select($table_name, $where, 'title', '', true,false,'*',array('str')));
        if (isAssoc($response)){
            $response=array($response);
        }

    } elseif ($_SERVER['REQUEST_METHOD'] === "POST") {
        if (is_array($_REQUEST) && array_key_exists('reset',$_REQUEST) && ($_REQUEST['reset'] == 'true')) {
            $response = $db->ExecuteSQL($queries->deleteAllRows) && $db->ExecuteSQL($queries->insertDefaultRows);
        } else {
            $movie = getRequestPayload();
            $response = $db->Insert($table_name, $movie,'',array('str','str','int','str'));
        }
    } elseif ($_SERVER['REQUEST_METHOD'] === "PUT") {
        $movie = getRequestPayload();
	    $response = $db->Update($table_name, $movie, array('id' => $movie['id']),'',array('int','str','int','str','str'),array('int'));
    } elseif ($_SERVER['REQUEST_METHOD'] === "DELETE") {
	    $response = $db->Delete($table_name, $_REQUEST,'',false,array('int'));
    }
    sendResponse($response);
?>
